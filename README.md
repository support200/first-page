# First Page

10 FEB First page for Speech Beacon website via GitLab services

11 FEB switched to HUGO Netlify template with CMS to try to launch the website.

22 FEB have changed the logo and added the posts but having difficulty customzing the headers and titles. Not sure whether gitlab or netlify or even if github is also involved since someone wrote the CMS on github and not on gitlab

25 FEB found the partials section which changes the header and footer and customizing them. Hope to fix the CSS so that the customization can be better. Did not figure out how to use HUGO on GitLab yet. Currently replying on Netlify. 

27 FEB trying to activate a GitLab pages to learn the hugo-gitlab method without netlify services. Not able to get the page to work. 

31 MAR developed the website using w3css and uploading the files to gitlab to try out the hosting of static pages